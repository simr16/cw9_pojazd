package pojazdPack;

public interface PojazdMechaniczny {
    public void setNrRejestracyjny(String nrRejestracyjny);

    public String getNrRejestracyjny();

    public String daneTechniczne();

    public void Przyjmij(Pracownik pracownik);

}
