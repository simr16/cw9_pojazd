package pojazdPack;

public class SpecDsModyfikacji implements Pracownik {

    @Override
    public void przeanalizujSamochod(Samochod samochod) {
	double a = samochod.getSilnik().getPojemnosc();
	double t = (a * 0.3) + a;
	System.out.println("Maksymalna dozwolona pojemno�� silnika dla samochodu " + samochod.getNazwa()
		+ " o pojemno�ci " + samochod.getSilnik() + " to " + t + "L");
    }

    @Override
    public void przeanalizujMotocykl(Motocykl motocykl) {
	double a = motocykl.getSilnik().getPojemnosc();
	double t = (a * 0.15) + a;
	System.out.println("Maksymalna dozwolona pojemno�� silnika dla motocykla " + motocykl.getNazwa()
		+ " o pojemno�ci " + motocykl.getSilnik() + " to " + t + "L");
    }

    @Override
    public void przeanalizujPrzyczepe(PrzyczepaKempingowa cemp) {
	System.out.println("Modyfikacje przyczep s� niedozwolone");

    }

}
