package pojazdPack;

import java.util.Random;

public class GeneratorRejestracji {

    public static String generator() {

	char a, b;
	int c;
	Random r = new Random();

	String slownik = "ABCDEFGHIJKLMNOPQRSTUWXYZ";

	a = slownik.charAt(r.nextInt(slownik.length()));
	b = slownik.charAt(r.nextInt(slownik.length()));

	do {
	    c = r.nextInt(9999);
	} while (c < 1000);

	return a + "" + b + c;
    }

}
