package pojazdPack;

public class Samochod extends Pojazd implements PojazdMechaniczny, ListaParkingowa {

    private Silnik silnik;
    private Kolo koloPrzedniePrawe, koloPrzednieLewe, koloTylnePrawe, koloTylneLewe;
    private PrzyczepaKempingowa camp;
    private String nrRejestracyjny;
    private int nrMiejsca;

    public Samochod(String nazwa, Silnik silnik, Kolo koloPrzedniePrawe, Kolo koloPrzednieLewe, Kolo koloTylnePrawe,
	    Kolo koloTylneLewe, PrzyczepaKempingowa kemping) {
	super(nazwa);
	this.silnik = silnik;
	this.koloPrzedniePrawe = koloPrzedniePrawe;
	this.koloPrzednieLewe = koloPrzednieLewe;
	this.koloTylnePrawe = koloTylnePrawe;
	this.koloTylneLewe = koloTylneLewe;
	this.camp = kemping;
    }

    public Samochod(String nazwa, Silnik silnik) {
	this(nazwa, silnik, new Kolo("SR20"), new Kolo("SR20"), new Kolo("SR20"), new Kolo("SR20"),
		new PrzyczepaKempingowa("Kemp"));
    }

    public Silnik getSilnik() {
	return silnik;
    }

    public void setSilnik(Silnik silnik) {
	this.silnik = silnik;
    }

    public Kolo getKoloPrzedniePrawe() {
	return koloPrzedniePrawe;
    }

    public void setKoloPrzedniePrawe(Kolo koloPrzedniePrawe) {
	this.koloPrzedniePrawe = koloPrzedniePrawe;
    }

    public Kolo getKoloPrzednieLewe() {
	return koloPrzednieLewe;
    }

    public void setKoloPrzednieLewe(Kolo koloPrzednieLewe) {
	this.koloPrzednieLewe = koloPrzednieLewe;
    }

    public Kolo getKoloTylnePrawe() {
	return koloTylnePrawe;
    }

    public void setKoloTylnePrawe(Kolo koloTylnePrawe) {
	this.koloTylnePrawe = koloTylnePrawe;
    }

    public Kolo getKoloTylneLewe() {
	return koloTylneLewe;
    }

    public void setKoloTylneLewe(Kolo koloTylneLewe) {
	this.koloTylneLewe = koloTylneLewe;
    }

    public PrzyczepaKempingowa getCamp() {
	return camp;
    }

    public void setCamp(PrzyczepaKempingowa camp) {
	this.camp = camp;
    }

    @Override
    public void setNrRejestracyjny(String nrRejestracyjny) {
	this.nrRejestracyjny = nrRejestracyjny;
    }

    public String getNrRejestracyjny() {
	return nrRejestracyjny;
    }

    public void setZaparkuj(int nrMiejsca) {
	this.nrMiejsca = nrMiejsca;
    }

    public int getZaparkuj() {
	return nrMiejsca;
    }

    public String daneTechniczne() {
	return "Nazwa: " + this.getNazwa() + " " + " Pojemno��: " + this.getSilnik().getPojemnosc()
		+ " Numer rejestracyjny: " + this.getNrRejestracyjny();

    }

    @Override
    public void Przyjmij(Pracownik pracownik) {
	pracownik.przeanalizujSamochod(this);

    }

}
