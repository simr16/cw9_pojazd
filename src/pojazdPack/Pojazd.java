package pojazdPack;

public abstract class Pojazd {

    private String nazwa;

    public Pojazd(String nazwa) {
	this.nazwa = nazwa;
    }

    public String getNazwa() {
	return nazwa;
    }

    public void setNazwa(String nazwa) {
	this.nazwa = nazwa;
    }

}
