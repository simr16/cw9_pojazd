package pojazdPack;

public class Silnik {

    private Double pojemnosc;

    public Silnik(Double pojemnosc) {
	this.pojemnosc = pojemnosc;
    }

    public Double getPojemnosc() {
	return pojemnosc;
    }

    public void setPojemnosc(Double pojemnosc) {
	this.pojemnosc = pojemnosc;
    }

    public String toString() {
	return this.getPojemnosc().toString();
    }

}
