package pojazdPack;

public class WozekBoczny extends Pojazd implements ListaParkingowa {

    private Kolo kolo;
    private int nrMiejsca;

    public WozekBoczny(String nazwa, Kolo kolo) {
	super(nazwa);
	this.kolo = kolo;
    }

    public WozekBoczny(String nazwa) {
	this(nazwa, new Kolo("WR10"));
    }

    public Kolo getKolo() {
	return kolo;
    }

    public void setKolo(Kolo kolo) {
	this.kolo = kolo;
    }

    @Override
    public void setZaparkuj(int nrMiejsca) {
	this.nrMiejsca = nrMiejsca;

    }

    @Override
    public int getZaparkuj() {
	return nrMiejsca;
    }

}
