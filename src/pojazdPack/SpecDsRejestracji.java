package pojazdPack;

public class SpecDsRejestracji implements Pracownik {

    @Override
    public void przeanalizujSamochod(Samochod samochod) {
	System.out.println("Nazwa: " + samochod.getNazwa() + " Pojemno��: " + samochod.getSilnik().getPojemnosc()
		+ " Numer rejestracyjny: " + samochod.getNrRejestracyjny());

    }

    @Override
    public void przeanalizujMotocykl(Motocykl motocykl) {
	System.out.println("Nazwa: " + motocykl.getNazwa() + " Pojemno��: " + motocykl.getSilnik().getPojemnosc()
		+ " Numer rejestracyjny: " + motocykl.getNrRejestracyjny());

    }

    @Override
    public void przeanalizujPrzyczepe(PrzyczepaKempingowa kemping) {
	System.out.println("Nazwa: " + kemping.getNazwa() + " Pojemno��: " + " Numer rejestracyjny: "
		+ kemping.getNrRejestracyjny());

    }

}
