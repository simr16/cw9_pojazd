package pojazdPack;

public class Motocykl extends Pojazd implements PojazdMechaniczny, ListaParkingowa {

    private Silnik silnik;
    private Kolo koloPrzednie;
    private Kolo koloTylne;
    private WozekBoczny woz;
    private String nrRejestracyjny;
    private int nrMiejsca;

    public Motocykl(String nazwa, Silnik silnik, Kolo koloPrzednie, Kolo koloTylne, WozekBoczny woz) {
	super(nazwa);
	this.silnik = silnik;
	this.koloPrzednie = koloPrzednie;
	this.koloTylne = koloTylne;
	this.woz = woz;
    }

    public Motocykl(String nazwa, Silnik silnik) {
	this(nazwa, silnik, new Kolo("MR10"), new Kolo("MR10"), new WozekBoczny("woz"));
    }

    public Silnik getSilnik() {
	return silnik;
    }

    public void setSilnik(Silnik silnik) {
	this.silnik = silnik;
    }

    public Kolo getKoloPrzednie() {
	return koloPrzednie;
    }

    public void setKoloPrzednie(Kolo koloPrzednie) {
	this.koloPrzednie = koloPrzednie;
    }

    public Kolo getKoloTylne() {
	return koloTylne;
    }

    public void setKoloTylne(Kolo koloTylne) {
	this.koloTylne = koloTylne;
    }

    public WozekBoczny getWoz() {
	return woz;
    }

    public void setWoz(WozekBoczny woz) {
	this.woz = woz;
    }

    @Override
    public void setNrRejestracyjny(String nrRejestracyjny) {
	this.nrRejestracyjny = nrRejestracyjny;
    }

    @Override
    public String getNrRejestracyjny() {
	return nrRejestracyjny;
    }

    public String daneTechniczne() {
	return "Nazwa: " + this.getNazwa() + " Pojemno��: " + this.getSilnik() + " Numer rejestracyjny: "
		+ this.getNrRejestracyjny();
    }

    @Override
    public void setZaparkuj(int nrMiejsca) {
	this.nrMiejsca = nrMiejsca;
    }

    @Override
    public int getZaparkuj() {
	return nrMiejsca;
    }

    @Override
    public void Przyjmij(Pracownik pracownik) {
	pracownik.przeanalizujMotocykl(this);
    }

}
