package pojazdPack;

public class PrzyczepaKempingowa extends Pojazd implements PojazdMechaniczny, ListaParkingowa {

    private Kolo koloPrawe, koloLewe;
    private String nrRejestracyjny;
    private int nrMiejsca;

    public PrzyczepaKempingowa(String nazwa, Kolo koloPrawe, Kolo koloLewe) {
	super(nazwa);
	this.koloPrawe = koloPrawe;
	this.koloLewe = koloLewe;
    }

    public PrzyczepaKempingowa(String nazwa) {
	this(nazwa, new Kolo("PR20"), new Kolo("PR20"));
    }

    public Kolo getKoloPrawe() {
	return koloPrawe;
    }

    public void setKoloPrawe(Kolo koloPrawe) {
	this.koloPrawe = koloPrawe;
    }

    public Kolo getKoloLewe() {
	return koloLewe;
    }

    public void setKoloLewe(Kolo koloLewe) {
	this.koloLewe = koloLewe;
    }

    @Override
    public void setNrRejestracyjny(String nrRejestracyjny) {
	this.nrRejestracyjny = nrRejestracyjny;
    }

    @Override
    public String getNrRejestracyjny() {
	return nrRejestracyjny;
    }

    public String daneTechniczne() {
	return "Nazwa: " + this.getNazwa() + " Numer rejestracyjny: " + this.getNrRejestracyjny();
    }

    @Override
    public void setZaparkuj(int nrMiejsca) {
	this.nrMiejsca = nrMiejsca;

    }

    @Override
    public int getZaparkuj() {
	return nrMiejsca;
    }

    @Override
    public void Przyjmij(Pracownik pracownik) {
	pracownik.przeanalizujPrzyczepe(this);

    }

}
