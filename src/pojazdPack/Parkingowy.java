package pojazdPack;

public class Parkingowy {
    String nazwisko;

    public Parkingowy(String nazwisko) {
	super();
	this.nazwisko = nazwisko;
    }

    public String getNazwisko() {
	return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
	this.nazwisko = nazwisko;
    }

    public void przypiszNr(ListaParkingowa lista) {
	lista.setZaparkuj(2);
    }

}
