package pojazdPack;

public interface Pracownik {
    public void przeanalizujSamochod(Samochod samochod);

    public void przeanalizujMotocykl(Motocykl motocykl);

    public void przeanalizujPrzyczepe(PrzyczepaKempingowa kemping);
}
