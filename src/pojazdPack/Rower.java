package pojazdPack;

public class Rower extends Pojazd implements ListaParkingowa {

    private Kolo koloPrzednie, koloTylne;
    private String nrRejestracyjny;
    private int nrMiejsca;

    public Rower(String nazwa, Kolo koloPrzednie, Kolo koloTylne) {
	super(nazwa);
	this.koloPrzednie = koloPrzednie;
	this.koloTylne = koloTylne;
    }

    public Kolo getKoloPrzednie() {
	return koloPrzednie;
    }

    public void setKoloPrzednie(Kolo koloPrzednie) {
	this.koloPrzednie = koloPrzednie;
    }

    public Kolo getKoloTylne() {
	return koloTylne;
    }

    public void setKoloTylne(Kolo koloTylne) {
	this.koloTylne = koloTylne;
    }

    public void setNrRejestracyjny(String nrRejestracyjny) {
	this.nrRejestracyjny = nrRejestracyjny;
	System.out.println("Przczep tablice z numerem rejestracyjnym " + this.nrRejestracyjny + " na ramie");
    }

    @Override
    public void setZaparkuj(int nrMiejsca) {
	this.nrMiejsca = nrMiejsca;

    }

    @Override
    public int getZaparkuj() {
	return nrMiejsca;
    }

}
