package pojazdPack;

public class Kolo {

    private String oznaczenie;

    public Kolo(String oznaczenie) {
	this.oznaczenie = oznaczenie;
    }

    public String getOznaczenie() {
	return oznaczenie;
    }

    public void setOznaczenie(String oznaczenie) {
	this.oznaczenie = oznaczenie;
    }

}