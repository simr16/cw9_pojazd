package pojazdPack;

import java.util.ArrayList;
import java.util.List;

public class UrzadKomunikacji {

	private int ukID;
	private String adres;
	private List<PojazdMechaniczny> rejestr;

	public UrzadKomunikacji(int ukID, String adres) {
		super();
		this.ukID = ukID;
		this.adres = adres;
		this.rejestr = new ArrayList<PojazdMechaniczny>();
	}

	public int getUkID() {
		return ukID;
	}

	public void setUkID(int ukID) {
		this.ukID = ukID;
	}

	public String getAdres() {
		return adres;
	}

	public void setAdres(String adres) {
		this.adres = adres;
	}

	public List<PojazdMechaniczny> getRejestr() {
		return rejestr;
	}

	public void setRejestr(List<PojazdMechaniczny> rejestr) {
		this.rejestr = rejestr;
	}

	public void zarejestrujPojazd(PojazdMechaniczny pojazdMechaniczny) {
		pojazdMechaniczny.setNrRejestracyjny(GeneratorRejestracji.generator());
		this.rejestr.add(pojazdMechaniczny);
	}

	public String toString() {
		String opis = "";

		for (PojazdMechaniczny pojazdMechaniczny : rejestr) {
			opis = opis + pojazdMechaniczny.daneTechniczne() + "\n";
		}

		return opis;

	}

	public void wyswietlZarejestrowanePojazdy() {

		SpecDsRejestracji sdr = new SpecDsRejestracji();
		for (PojazdMechaniczny pojazdMechaniczny : rejestr) {
			pojazdMechaniczny.Przyjmij(sdr);
		}

	}

	public void wyswietlDozwoloneModyfikacje() {

		SpecDsModyfikacji sdm = new SpecDsModyfikacji();
		for (PojazdMechaniczny pojazdMechaniczny : rejestr) {
			pojazdMechaniczny.Przyjmij(sdm);
		}

	}

}
