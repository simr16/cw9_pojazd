package testPojazdPack;

import org.junit.Test;

import pojazdPack.Motocykl;
import pojazdPack.PrzyczepaKempingowa;
import pojazdPack.Samochod;
import pojazdPack.Silnik;
import pojazdPack.UrzadKomunikacji;

public class pojazdTest {

    @Test
    public void zarejestrujListaTest() {

	UrzadKomunikacji urzad = new UrzadKomunikacji(12, "Wiktorska");

	Samochod samochod = new Samochod("BMW", new Silnik(2.1));
	urzad.zarejestrujPojazd(samochod);

	Motocykl motocykl = new Motocykl("Ducati", new Silnik(0.8));
	urzad.zarejestrujPojazd(motocykl);

	PrzyczepaKempingowa przyczepa = new PrzyczepaKempingowa("Camper");
	urzad.zarejestrujPojazd(przyczepa);

	System.out.println(urzad);
    }

    @Test
    public void odwiedzajacy() {

	UrzadKomunikacji urzad = new UrzadKomunikacji(12, "Wiktorska");

	Samochod samochod = new Samochod("BMW", new Silnik(2.1));
	urzad.zarejestrujPojazd(samochod);

	Motocykl motocykl = new Motocykl("Ducati", new Silnik(0.8));
	urzad.zarejestrujPojazd(motocykl);

	PrzyczepaKempingowa przyczepa = new PrzyczepaKempingowa("Camper");
	urzad.zarejestrujPojazd(przyczepa);

	urzad.wyswietlZarejestrowanePojazdy();
	System.out.println("");
	urzad.wyswietlDozwoloneModyfikacje();

    }

}
